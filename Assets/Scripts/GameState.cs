﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// this handles the state of the game
/// </summary>
public class GameState : Core.Utilities.Singleton<GameState> {
    public enum GameStatus { Intro, InGame, PanelShow, Paused, GameOver}
    public GameStatus _currentState = GameStatus.Intro;
    public GameStatus GetStats { get { return _currentState; } }
    public float WaitTime;
    public Transform GoTxt;

    void ChangeState(GameStatus nextState)
    {
        _currentState = nextState;
        Event_GameStatusChanged e = new Event_GameStatusChanged(_currentState);
        this.RaiseEventGlobal<Event_GameStatusChanged>(e);
    }
    private void Start()
    {
        StartCoroutine(WaitToStart(WaitTime));
    }
    public IEnumerator WaitToStart(float time)
    {
        yield return new WaitForSeconds(time);
        ChangeState(GameStatus.InGame);

    }

}
public class Event_GameStatusChanged : GameEvent
{
    public GameState.GameStatus s;
    public Event_GameStatusChanged()
    {

    }
    public Event_GameStatusChanged(GameState.GameStatus Status)
    {
        s = Status;
    }

}

