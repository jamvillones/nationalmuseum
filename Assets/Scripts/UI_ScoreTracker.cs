﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ScoreTracker : MonoBehaviour
{
    public Text tries;
    public Text ScoreTxt;

    // Use this for initialization
    void Start()
    {

        ScoreStatistics.instance.NumTries = SelectionManager.instance.LatterCards.Count;
        tries.text = ScoreStatistics.instance.NumTries.ToString();

        ScoreTxt.text = ScoreStatistics.instance.Score.ToString();


    }
    private void OnEnable()
    {
        this.AddEventListenerGlobal<Event_SelectResult>(Event_SelectResult_CB);
    }
    private void OnDisable()
    {
        this.RemoveEventListenerGlobal<Event_SelectResult>(Event_SelectResult_CB);
    }
    void Event_SelectResult_CB(Event_SelectResult e)
    {
       
        if (e.Correct)
        {
            ScoreStatistics.instance.NumTries--;
            tries.text = ScoreStatistics.instance.NumTries.ToString();

            ScoreStatistics.instance.Score += 10;
            ScoreChanged sc = new ScoreChanged();
            sc.type = ScoreChanged.ScoreType.both;
            this.RaiseEventGlobal<ScoreChanged>(sc);
            ScoreTxt.text = ScoreStatistics.instance.Score.ToString();

           
            //if (ScoreStatistics.instance.NumTries <= 0)
            //{
            //    GameResult g = new GameResult(ScoreStatistics.instance.Score);
            //    this.RaiseEventGlobal<GameResult>(g);
            //}

        }
        else if(!e.Correct)
        {
            ScoreStatistics.instance.NumTries--;

            ScoreChanged sc = new ScoreChanged();
            sc.type = ScoreChanged.ScoreType.scoreOnly;
            this.RaiseEventGlobal<ScoreChanged>(sc);


            tries.text = ScoreStatistics.instance.NumTries.ToString();

            //if (ScoreStatistics.instance.NumTries <= 0)
            //{
            //    GameResult g = new GameResult(ScoreStatistics.instance.Score);
            //    this.RaiseEventGlobal<GameResult>(g);
            //}
        }




    }
}
public class GameResult : GameEvent
{
    public int score;
    public GameResult()
    {

    }
    public GameResult(int _score)
    {
        score = _score;
    }
}
public class ScoreChanged : GameEvent
{
    public enum ScoreType { scoreOnly, both }
    public ScoreType type;
}
