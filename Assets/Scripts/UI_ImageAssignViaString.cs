﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ImageAssignViaString : MonoBehaviour {
    Card1 card;
    [SerializeField]
    Texture2D t;
    Image imageComponent;

    private void Awake()
    {
        imageComponent = GetComponent<Image>(); 
        card = GetComponent<Card1>();
    }

    // Use this for initialization
    void Start () {
        Set();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void Set()
    {
        t = Resources.Load(card.infos.Name) as Texture2D;
        imageComponent.sprite = Sprite.Create(t, new Rect(0.0f, 0.0f, t.width, t.height), new Vector2(0.5f, 0.5f));
    }
}
