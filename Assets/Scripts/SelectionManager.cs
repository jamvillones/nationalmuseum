﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SelectionManager : Core.Utilities.Singleton<SelectionManager>
{
    public GameObject LineConnector;
    public GameObject TopSideCardsParent;
    public GameObject BottomSideCardsParent;
    public GameObject IsolatedParent;
    [HideInInspector]
    public GameObject SelectedCard;
    [HideInInspector]
    public GameObject HoveredCard;
    [HideInInspector]
    public List<Card> FormerCards;
    [HideInInspector]
    public List<Card> LatterCards;
    GameObject activeLine;
    List<GameObject> isolatedBones = new List<GameObject>();
    List<GameObject> isolatedAnimals = new List<GameObject>();
    bool locked = false;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
        foreach (Transform child in TopSideCardsParent.transform)
        {
            if (child.GetComponent<Card>())
                FormerCards.Add(child.GetComponent<Card>());
        }
        foreach (Transform child in BottomSideCardsParent.transform)
        {
            if (child.GetComponent<Card>())
                LatterCards.Add(child.GetComponent<Card>());
        }
        // to fix by bert

        //FormerCards.ForEach(c => c.manager = this);
        //LatterCards.ForEach(c => c.Manager = this);
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))
        //{
        //    locked = true;
        //    if (HoveredCard)
        //    {
        //        if (!inLine(HoveredCard))
        //            HoveredCard.GetComponent<Card>().Evaluate();
        //    }
        //    Destroy(activeLine);
        //    if (SelectedCard)
        //        SelectedCard.GetComponent<Animator>().SetBool("Selected", false);
        //    SelectedCard = null;
        //    locked = false;
        //}
    }

    bool inLine(GameObject comparison)
    {
        if (SelectedCard)
        {
            if (FormerCards.Exists(c => c.Equals(SelectedCard.GetComponent<Card>())) && FormerCards.Exists(c => c.Equals(comparison.GetComponent<Card>())))
                return true;

            else if (LatterCards.Exists(c => c.Equals(SelectedCard.GetComponent<Card>())) && LatterCards.Exists(c => c.Equals(comparison.GetComponent<Card>())))
                return true;
        }
        return false;
    }

    public void ShuffleAnimals()
    {
        TopSideCardsParent.GetComponent<ChildShuffler>().ShuffleChildren();
        BottomSideCardsParent.GetComponent<ChildShuffler>().ShuffleChildren();
    }

    public void OnClickPrompt(GameObject caller)
    {
        locked = false;
        if (GameState.instance._currentState != GameState.GameStatus.InGame)
            return;

        if (caller.Equals(SelectedCard))
        {
            caller.GetComponent<Animator>().SetBool("Selected", false);
            Destroy(activeLine);
            SelectedCard = null;
        }
        if (!activeLine)
        {
            // caller.GetComponent<Animator>().SetBool("Selected", true);
            SelectedCard = caller;
            SpawnLine(caller.transform.position);
        }
    }


    public void TemporaryIsolation()
    {
        isolatedAnimals.ForEach(o => o.transform.SetParent(TopSideCardsParent.transform));
        isolatedBones.ForEach(o => o.transform.SetParent(BottomSideCardsParent.transform));
        isolatedAnimals.Clear();
        isolatedBones.Clear();
        isolatedAnimals.TrimExcess();
        isolatedBones.TrimExcess();
        if (FormerCards.Exists(c => c.Equals(HoveredCard.GetComponent<Card>())))
        {
            isolatedBones.Add(SelectedCard);
            isolatedAnimals.Add(HoveredCard);
        }
        else
        {
            isolatedBones.Add(HoveredCard);
            isolatedAnimals.Add(SelectedCard);
        }
        isolatedAnimals.ForEach(o => o.transform.SetParent(IsolatedParent.transform));
        isolatedBones.ForEach(o => o.transform.SetParent(IsolatedParent.transform));
        //ShuffleAnimals();
    }

    public void CorrectAnswerCleanup(GameObject evaluatedCard)
    {
        if (FormerCards.Exists(c => c.Equals(evaluatedCard.GetComponent<Card>())))
        {
            //FormerCards.RemoveAt(FormerCards.FindIndex(c => c.Equals(evaluatedCard.GetComponent<Card>())));
            LatterCards.RemoveAt(LatterCards.FindIndex(c => c.Equals(evaluatedCard.GetComponent<Card>().CorrectPartner.GetComponent<Card>())));
            Destroy(evaluatedCard.GetComponent<Card>().CorrectPartner);
        }
        else
        {
            //FormerCards.RemoveAt(FormerCards.FindIndex(c => c.Equals(evaluatedCard.GetComponent<Card>().CorrectPartner.GetComponent<Card>())));
            LatterCards.RemoveAt(LatterCards.FindIndex(c => c.Equals(evaluatedCard.GetComponent<Card>())));
            Destroy(evaluatedCard);
        }
        // FormerCards.TrimExcess();
        LatterCards.TrimExcess();
        Destroy(activeLine);
    }

    public void MaintainCurrentLine()
    {
        activeLine.GetComponent<LineFollow>().UpdatePosition = false;
        activeLine = null;
    }

    public void SpawnLine(Vector3 position)
    {
        if (activeLine)
            Destroy(activeLine);
        GameObject temp = Instantiate(LineConnector, position, Quaternion.identity);
        activeLine = temp;
    }

    public void SnapToCardPosition()
    {
        if (GameState.instance._currentState != GameState.GameStatus.InGame)
            return;

        if (!inLine(HoveredCard))
            HoveredCard.GetComponent<Animator>().SetBool("Selected", true);
        if (activeLine && !inLine(HoveredCard))
        {
            LineRenderer lineRenderer = activeLine.GetComponent<LineRenderer>();
            LineFollow lineFollow = activeLine.GetComponent<LineFollow>();
            lineFollow.UpdatePosition = false;
            lineRenderer.positionCount = 4;
            lineRenderer.SetPosition(1, new Vector3(SelectedCard.transform.position.x, FormerCards.Exists(c => c.Equals(SelectedCard.GetComponent<Card>())) ? SelectedCard.transform.position.y + 1 : SelectedCard.transform.position.y - 1, SelectedCard.transform.position.z));
            lineRenderer.SetPosition(2, new Vector3(HoveredCard.transform.position.x, LatterCards.Exists(c => c.Equals(HoveredCard.GetComponent<Card>())) ? HoveredCard.transform.position.y - 1 : HoveredCard.transform.position.y + 1, HoveredCard.transform.position.z));
            lineRenderer.SetPosition(3, HoveredCard.transform.position);
        }
    }

    public void OnPointerExit()
    {
        if (SelectedCard ? !SelectedCard.Equals(HoveredCard) : true)
            HoveredCard.GetComponent<Animator>().SetBool("Selected", false);
        if (activeLine)
        {
            activeLine.GetComponent<LineRenderer>().positionCount = 3;
            activeLine.GetComponent<LineFollow>().UpdatePosition = true;
        }
            HoveredCard = null;
    }

    public void voidOnMouseUP(GameObject caller)
    {
        if (HoveredCard)
        {
            if (!inLine(HoveredCard))
                HoveredCard.GetComponent<Card>().Evaluate();
        }
        Destroy(activeLine);
        if (SelectedCard)
            SelectedCard.GetComponent<Animator>().SetBool("Selected", false);
        SelectedCard = null;
    }

}
