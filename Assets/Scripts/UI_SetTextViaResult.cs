﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SetTextViaResult : MonoBehaviour
{
    public Text[] TextComponent;

    // Use this for initialization
    void Start()
    {
        TextComponent = GetComponentsInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnEnable()
    {
        this.AddEventListenerGlobal<Event_SelectResult>(Event_SelectResult_CB);
    }
    private void OnDisable()
    {
        this.RemoveEventListenerGlobal<Event_SelectResult>(Event_SelectResult_CB);
    }
    void Event_SelectResult_CB(Event_SelectResult e)
    {
        if (e.Correct)
        {
            TextComponent[0].text = e.animalinfo.Name;
            TextComponent[1].text = e.animalinfo.Description;
        }
        else
        {
            TextComponent[0].text = "Bones do not match!";
            TextComponent[1].text = "Oops, Try Again!";
        }
    }
}
