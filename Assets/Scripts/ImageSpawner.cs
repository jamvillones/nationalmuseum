﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageSpawner : MonoBehaviour {
    public float Ypoint;
    public Transform ImageToBeSpawned;
    public Texture2D texture;
    Transform t;
    public float Speed = 5;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (t == null)
            return;
        // t.position = Vector3.MoveTowards(t.position,this.transform.position,Speed*Time.deltaTime);
        t.position = Vector3.Lerp(t.position, transform.position, Speed * Time.deltaTime);
        if (Vector2.Distance(t.position, transform.position) <= 0)
        {
            this.enabled = false;
        }
           
		
	}
    public void Spawn()
    {
        if (ImageToBeSpawned == null || Ypoint == 0)
        {
            Debug.Log("please assign the image to be spawned or make sure the spawn point is not (0,0)");
            return;
        }
        t = Instantiate(ImageToBeSpawned, new Vector3(transform.position.x, Ypoint, transform.position.z), Quaternion.identity);
        t.GetComponent<UnityEngine.UI.Image>().sprite = Sprite.Create(texture, new Rect(0.0f,0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        t.SetParent(this.transform);
        t.localScale = this.transform.localScale;

    }
}
