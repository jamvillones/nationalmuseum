﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class TemporaryDataSetter : MonoBehaviour {
    public DataHolder dh;

	// Use this for initialization
	void Start () {
        dh.AnimalInfos = this.LoadArray<AnimalInfo>("/AnimalInfo.dat").ToList();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
