﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "DataHolder")]
public class DataHolder : ScriptableObject {
    public List<AnimalInfo> AnimalInfos;
}
