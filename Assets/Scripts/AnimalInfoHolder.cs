﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Utilities;
using System.Linq;

public class AnimalInfoHolder : Singleton<AnimalInfoHolder>
{
    public DataHolder dataHolder;
}
public class DataChanged : GameEvent
{
    public enum ChangeType { Saved, Loaded }
    public ChangeType type;
}

[System.Serializable]
public class AnimalInfo
{
    public string Name;
    public string Description;
    public string PartialDescription;
    public Texture2D Sprite;
}

