﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// this class handles the close and open animation
/// </summary>

public class UI_AnimalInfo_Trigger : MonoBehaviour
{
    public GameObject OverlayPanel;
    Animator animator;
    Button backBtn;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        backBtn = GetComponentInChildren<Button>();
        OverlayPanel.SetActive(false);
    }
    private void OnEnable()
    {
        if (backBtn != null)
            backBtn.onClick.AddListener(delegate { BackCB(); });

    }

    private void OnDisable()
    {
        if (backBtn != null)
            backBtn.onClick.RemoveListener(delegate { BackCB(); });
    }



    public void BackCB()
    {
        OverlayPanel.SetActive(false);
        animator.SetBool("Open", false);
    }
    public void OpenCB()
    {
        OverlayPanel.SetActive(true);
        animator.SetBool("Open", true);
    }
}
